package andriod.romon.performanceassessment;

public class ToDoItem {

    public String title;
    public String dateAdded;
    public String dateDue;
    public String category;

    public ToDoItem(String title, String dateAdded, String dateDue, String category){
        this.title = title;
        this.dateAdded = dateAdded;
        this.dateDue = dateDue;
        this.category = category;

    }

}

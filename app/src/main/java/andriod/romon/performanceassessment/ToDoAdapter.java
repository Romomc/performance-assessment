package andriod.romon.performanceassessment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class ToDoAdapter extends RecyclerView.Adapter<ToDoHolder> {
    private ActivityCallBack activityCallBack;
    private ArrayList<ToDoItem> toDoItems;
    private ToDoAdapter holder;

    public ToDoAdapter(ActivityCallBack activityCallBack, ArrayList<ToDoItem> toDoItems) {
        this.activityCallBack = activityCallBack;
        this.toDoItems = toDoItems;
    }

    public ArrayList<ToDoItem> getToDoItems(){
        return toDoItems;
    }


    @Override
    public ToDoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new ToDoHolder(view);
    }

    @Override
    public void onBindViewHolder(ToDoHolder holder, final int position) {
       holder.titleText.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               activityCallBack.onPostSelected(position);

           }
       });
        holder.titleText.setText(toDoItems.get(position).title);
    }

    @Override
    public int getItemCount() {
        return toDoItems.size();
    }
}

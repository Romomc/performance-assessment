package andriod.romon.performanceassessment;

import android.app.FragmentTransaction;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;

public class MainActivity extends SingleFragmentActivity implements ActivityCallBack{
    public int currentItem;
    ArrayList<ToDoItem> toDoItem = new ArrayList<>();

    @Override
    protected Fragment createFragment() {
        return new ToDoFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPostSelected(int pos) {
        currentItem = pos;
        Fragment newFragment = new ItemFragment();

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);

        transaction.addToBackStack(null);
        transaction.commit();
    }
}

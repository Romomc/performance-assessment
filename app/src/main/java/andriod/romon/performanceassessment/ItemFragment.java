package andriod.romon.performanceassessment;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ItemFragment extends Fragment {
    private MainActivity activity;
    private RecyclerView recyclerView;
    private ActivityCallBack activityCallBack;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallBack = (ActivityCallBack) activity;
        this.activity = (MainActivity) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallBack = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.item_fragment, container, false);

        TextView i1 = (TextView) layoutView.findViewById(R.id.hello);
        i1.setText("Name: " + activity.toDoItem.get(activity.currentItem).title);

        TextView i2 = (TextView) layoutView.findViewById(R.id.hey);
        i2.setText("DateAdded: " + activity.toDoItem.get(activity.currentItem).dateAdded);

        TextView i3 = (TextView) layoutView.findViewById(R.id.bye);
        i3.setText("DateDue: " + activity.toDoItem.get(activity.currentItem).dateDue);

        TextView i4 = (TextView) layoutView.findViewById(R.id.goodbye);
        i4.setText("Category: " + activity.toDoItem.get(activity.currentItem).category);

        return layoutView;
    }
}

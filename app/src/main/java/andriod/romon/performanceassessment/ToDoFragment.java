package andriod.romon.performanceassessment;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ToDoFragment extends Fragment{

    private MainActivity activity;
    private RecyclerView recyclerView;
    private ActivityCallBack activityCallBack;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallBack = (ActivityCallBack)activity;
        this.activity = (MainActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallBack = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_layout, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ToDoItem i1 = new ToDoItem("Hey", "How", "Are", "You");
        ToDoItem i2 = new ToDoItem("I", "Could", "Care", "Less");
        ToDoItem i3 = new ToDoItem("Go", "Away", "From", "Here");
        ToDoItem i4 = new ToDoItem("The", "Textbooks", "I", "Need");

        activity.toDoItem.add(i1);
        activity.toDoItem.add(i2);
        activity.toDoItem.add(i3);
        activity.toDoItem.add(i4);

        ToDoAdapter adapter = new ToDoAdapter(activityCallBack, activity.toDoItem);
        recyclerView.setAdapter(adapter);

        return view;


    }
}
